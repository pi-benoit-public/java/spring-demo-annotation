package fr.benoit.annotation;

public interface ICoach {
	public String getDailyWorkout();

	public String getDailyFortune();
}
