package fr.benoit.annotation;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements IFortuneService {

	private String[] msgAleatoire = new String[] {
		"Allez",
		"Plus vite",
		"Plus fort",
		"Respirez",
		"Toujours plus haut"
	};

	@Override
	public String getDailyFortune() {
		Random myRandom = new Random();
		int aleatoire = myRandom.nextInt(this.msgAleatoire.length);
		return this.msgAleatoire[aleatoire];
	}

}
