package fr.benoit.annotation;

import org.springframework.stereotype.Component;

@Component
public class EatingFortuneService implements IFortuneService {

	@Override
	public String getDailyFortune() {
		return "Manger !";
	}

}
