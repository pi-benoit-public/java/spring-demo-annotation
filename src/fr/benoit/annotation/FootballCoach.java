package fr.benoit.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FootballCoach implements ICoach {

	@Value("${foo.email}")
	private String email;

	@Value("${foo.team}")
	private String team;

	@Autowired
	@Qualifier("randomFortuneService")
	private IFortuneService ifs;

	public FootballCoach() {
		super();
	}

	public FootballCoach(String email, String team, IFortuneService ifs) {
		super();
		this.email = email;
		this.team = team;
		this.ifs = ifs;
	}

	@Override
	public String getDailyWorkout() {
		return "Pause";
	}

	@Override
	public String getDailyFortune() {
		return ifs.getDailyFortune();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public IFortuneService getIfs() {
		return ifs;
	}

	public void setIfs(IFortuneService ifs) {
		this.ifs = ifs;
	}

}
