package fr.benoit.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:infos.properties")
public class ConfigurationCoach {

	@Bean
	public ICoach baseballCoach() {
		return new BaseballCoach();
	}

	@Bean
	public ICoach footballCoach() {
		return new FootballCoach();
	}

	@Bean
	public IFortuneService happyFortuneService() {
		return new HappyFortuneService();
	}

	@Bean
	public IFortuneService randomFortuneService() {
		return new RandomFortuneService();
	}

}
