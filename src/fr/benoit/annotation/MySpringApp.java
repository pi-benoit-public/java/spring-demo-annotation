package fr.benoit.annotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MySpringApp {

	public static void main(String[] args) {

		// Avec Spring annotation.
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationCoach.class);

		ICoach cbb = context.getBean("baseballCoach", ICoach.class);

		System.out.println( cbb.getDailyWorkout() );
		System.out.println( cbb.getDailyFortune() );

		FootballCoach cf = context.getBean("footballCoach", FootballCoach.class);

		System.out.println( cf.getDailyWorkout() );
		System.out.println( cf.getDailyFortune() );
		System.out.println( cf.getEmail() );
		System.out.println( cf.getTeam() );

		context.close();

	}

}
