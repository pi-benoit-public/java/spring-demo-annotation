package fr.benoit.annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//Par d�faut le component c'est ici baseballCoach
@Component
// @ComponentScanning("idDeMonChoix") // ceci est optionnel - si l'on veut changer l'id.
@Scope("prototype")
public class BaseballCoach implements ICoach {

	// Annotation sur l'attribut de classe pour choisir le service voulu.
	@Autowired
	@Qualifier("randomFortuneService")
	IFortuneService ifs;

	// Constructeur par d�faut obligatoire si plusieurs services.
	public BaseballCoach() {
		System.out.println("BaseballCoach - Check adresse = "+ this.hashCode());
	}

	public BaseballCoach(IFortuneService ifs) {
		this.ifs = ifs;
	}

	@Override
	public String getDailyWorkout() {
		return "Fais 10 tours de la base.";
	}

	@Override
	public String getDailyFortune() {
		return ifs.getDailyFortune();
	}

}
