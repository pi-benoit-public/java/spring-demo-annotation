package fr.benoit.annotation;

import org.springframework.stereotype.Component;

@Component
public class HappyFortuneService implements IFortuneService {

	@Override
	public String getDailyFortune() {
		return "Joyeuses P�ques !";
	}

}
