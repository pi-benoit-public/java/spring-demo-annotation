package fr.benoit.annotationEtxml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MySpringApp {

	public static void main(String[] args) {

		// Avec Spring annotation.
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		ICoach cbb = context.getBean("baseballCoach", ICoach.class);
		ICoach cbb2 = context.getBean("baseballCoach", ICoach.class);

		System.out.println( cbb.getDailyWorkout() );
		System.out.println( cbb.getDailyFortune() );
		System.out.println( cbb2.getDailyWorkout() );
		System.out.println( cbb2.getDailyFortune() );

		ICoach cf = context.getBean("footballCoach", ICoach.class);
		ICoach cf2 = context.getBean("footballCoach", ICoach.class);

		System.out.println( cf.getDailyWorkout() );
		System.out.println( cf.getDailyFortune() );
		System.out.println( cf2.getDailyWorkout() );
		System.out.println( cf2.getDailyFortune() );

		context.close();

	}

}
