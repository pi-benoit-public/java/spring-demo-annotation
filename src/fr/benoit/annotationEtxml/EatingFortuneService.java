package fr.benoit.annotationEtxml;

import org.springframework.stereotype.Component;

@Component
public class EatingFortuneService implements IFortuneService {

	@Override
	public String getDailyFortune() {
		return "Manger !";
	}

}
