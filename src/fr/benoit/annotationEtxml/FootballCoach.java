package fr.benoit.annotationEtxml;

import javax.annotation.PostConstruct;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton") // Par d�faut
public class FootballCoach implements ICoach {

	@Autowired
	@Qualifier("happyFortuneService")
	IFortuneService ifs;

	public FootballCoach() {
		super();
		System.out.println("FootballCoach - Check adresse = "+ this.hashCode());
	}

	public FootballCoach(IFortuneService ifs) {
		super();
		this.ifs = ifs;
	}

	@PostConstruct
	public void myInitMethod() {
		JOptionPane.showMessageDialog(null, "Le coach football s'initialise.");
	}

	@Override
	public String getDailyWorkout() {
		return "10 tours du terrain de foot.";
	}

	@Override
	public String getDailyFortune() {
		return ifs.getDailyFortune();
	}

	public IFortuneService getIfs() {
		return ifs;
	}

	@Autowired
	@Qualifier("happyFortuneService")
	public void setIfs(IFortuneService ifs) {
		this.ifs = ifs;
	}

}
