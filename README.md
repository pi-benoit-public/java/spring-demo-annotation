# Projet avec Java avec Spring et annotation

[Voir pour l'installation des libs de Spring](https://gitlab.com/pi-benoit-public/java/spring-demo)

Deux packages :

1 - fr.benoit.annotationEtxml : mix spring annotation et xml

2 - fr.benoit.annotation : uniquement spring par annotation
